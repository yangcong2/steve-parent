#!/bin/bash

#测试token = fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171
#正式token = fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171

_DINGTALK_TOKEN=fb63f2534952c9b233530ea7cdeec93fdb88f566b0c70a24fb2dddd0dfa06171

if [ -z "${CI}" ] ; then
	echo "CI empty!"
	exit 99
fi

_CUR_DIR=$(cd $(dirname "${BASH_SOURCE[0]}" ) >/dev/null && pwd )
_PRJ_DIR=$(dirname ${_CUR_DIR})
_FILE=${_PRJ_DIR}/target/.flattened-pom.xml

_VERSION=`grep -Po '(?<=<version>).*?(?=</version>)' ${_FILE} |sed -n '1p'`

_TITLE="[parent项目发布]"

_TEXT="### 项目：${CI_PROJECT_NAME} 最新正式版已发布  \n
#### 组件包引用  \n
> &lt;parent&gt;  \n
> &nbsp;&nbsp;&lt;groupId&gt;com.huixian&lt;/groupId&gt;  \n
> &nbsp;&nbsp;&lt;artifactId&gt;huixian-parent2-infra&lt;/artifactId&gt;  \n
> &nbsp;&nbsp;&lt;version&gt;${_VERSION}&lt;/version&gt;  \n
> &lt;/parent&gt;  \n
  \n
#### 业务包引用  \n
> &lt;parent&gt;  \n
> &nbsp;&nbsp;&lt;groupId&gt;com.huixian&lt;/groupId&gt;  \n
> &nbsp;&nbsp;&lt;artifactId&gt;huixian-parent2-business&lt;/artifactId&gt;  \n
> &nbsp;&nbsp;&lt;version&gt;${_VERSION}&lt;/version&gt;  \n
> &lt;/parent&gt;  \n"

_JSON="{\"msgtype\": \"markdown\", \"markdown\": {\"title\": \"${_TITLE}\", \"text\": \"${_TEXT}\"}}"

curl -s "https://oapi.dingtalk.com/robot/send?access_token=${_DINGTALK_TOKEN}" -H 'Content-Type: application/json' --data-binary "${_JSON}"
echo
echo

exit 0
